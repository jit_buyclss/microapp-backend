package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.Order;
import cn.buyclss.microapp.entity.domain.OrderExample;
import java.util.List;

import cn.buyclss.microapp.entity.po.OrdersWithShopAndSpecsAndStatusPO;
import org.apache.ibatis.annotations.Param;

public interface OrderMapper {
    /**
     *
     * @mbg.generated 2019-03-22
     */
    long countByExample(OrderExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int deleteByExample(OrderExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int insert(Order record);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int insertSelective(Order record);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    List<Order> selectByExample(OrderExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    Order selectByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByExampleSelective(@Param("record") Order record, @Param("example") OrderExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByExample(@Param("record") Order record, @Param("example") OrderExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByPrimaryKeySelective(Order record);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByPrimaryKey(Order record);

    List<OrdersWithShopAndSpecsAndStatusPO> selectOrdersWithShopAndSpecsAndStatusByCustomId(Integer uid);
}