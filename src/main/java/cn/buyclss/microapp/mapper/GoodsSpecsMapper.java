package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.GoodsSpecs;
import cn.buyclss.microapp.entity.domain.GoodsSpecsExample;
import java.util.List;

import cn.buyclss.microapp.entity.dto.GoodsSpecsInsertDTO;
import cn.buyclss.microapp.entity.dto.UpdateGoodsSpecsDTO;
import org.apache.ibatis.annotations.Param;

public interface GoodsSpecsMapper {
    /**
     *
     * @mbg.generated 2019-03-20
     */
    long countByExample(GoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByExample(GoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insert(GoodsSpecs record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insertSelective(GoodsSpecs record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    List<GoodsSpecs> selectByExample(GoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    GoodsSpecs selectByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExampleSelective(@Param("record") GoodsSpecs record, @Param("example") GoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExample(@Param("record") GoodsSpecs record, @Param("example") GoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKeySelective(GoodsSpecs record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKey(GoodsSpecs record);

    void reduceStockById(UpdateGoodsSpecsDTO updateGoodsSpecsDTO);

    int addBatch(List<GoodsSpecsInsertDTO> goodsSpecsInsertDTOS);
}