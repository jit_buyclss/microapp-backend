package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.Goods;
import cn.buyclss.microapp.entity.domain.GoodsExample;
import java.util.List;

import cn.buyclss.microapp.entity.po.GoodsPO;
import cn.buyclss.microapp.entity.po.GoodsWithSpecsDetailPO;
import org.apache.ibatis.annotations.Param;

public interface GoodsMapper {
    /**
     *
     * @mbg.generated 2019-03-20
     */
    long countByExample(GoodsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByExample(GoodsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insert(Goods record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insertSelective(Goods record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    List<Goods> selectByExample(GoodsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    Goods selectByPrimaryKey(Integer id);

    /**
     * 连接shop category查询
     * @return
     */
    List<GoodsPO> selectGoodsWithShopAndCategory();

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKeySelective(Goods record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKey(Goods record);


    GoodsWithSpecsDetailPO selectGoodWithSpecs(Integer goodsId);
}