package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.GoodsStatus;
import cn.buyclss.microapp.entity.domain.GoodsStatusExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GoodsStatusMapper {
    /**
     *
     * @mbg.generated 2019-03-20
     */
    long countByExample(GoodsStatusExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByExample(GoodsStatusExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insert(GoodsStatus record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insertSelective(GoodsStatus record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    List<GoodsStatus> selectByExample(GoodsStatusExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    GoodsStatus selectByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExampleSelective(@Param("record") GoodsStatus record, @Param("example") GoodsStatusExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExample(@Param("record") GoodsStatus record, @Param("example") GoodsStatusExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKeySelective(GoodsStatus record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKey(GoodsStatus record);
}