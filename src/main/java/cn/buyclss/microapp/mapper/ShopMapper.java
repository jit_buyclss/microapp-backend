package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.Shop;
import cn.buyclss.microapp.entity.domain.ShopExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ShopMapper {
    /**
     *
     * @mbg.generated 2019-03-20
     */
    long countByExample(ShopExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByExample(ShopExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insert(Shop record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insertSelective(Shop record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    List<Shop> selectByExample(ShopExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    Shop selectByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExampleSelective(@Param("record") Shop record, @Param("example") ShopExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExample(@Param("record") Shop record, @Param("example") ShopExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKeySelective(Shop record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKey(Shop record);
}