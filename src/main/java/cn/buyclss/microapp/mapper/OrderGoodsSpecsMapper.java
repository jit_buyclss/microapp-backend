package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.OrderGoodsSpecs;
import cn.buyclss.microapp.entity.domain.OrderGoodsSpecsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrderGoodsSpecsMapper {
    /**
     *
     * @mbg.generated 2019-03-22
     */
    long countByExample(OrderGoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int deleteByExample(OrderGoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int insert(OrderGoodsSpecs record);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int insertSelective(OrderGoodsSpecs record);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    List<OrderGoodsSpecs> selectByExample(OrderGoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    OrderGoodsSpecs selectByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByExampleSelective(@Param("record") OrderGoodsSpecs record, @Param("example") OrderGoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByExample(@Param("record") OrderGoodsSpecs record, @Param("example") OrderGoodsSpecsExample example);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByPrimaryKeySelective(OrderGoodsSpecs record);

    /**
     *
     * @mbg.generated 2019-03-22
     */
    int updateByPrimaryKey(OrderGoodsSpecs record);
}