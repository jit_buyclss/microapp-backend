package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.GoodsCategory;
import cn.buyclss.microapp.entity.domain.GoodsCategoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GoodsCategoryMapper {
    /**
     *
     * @mbg.generated 2019-03-20
     */
    long countByExample(GoodsCategoryExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByExample(GoodsCategoryExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int deleteByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insert(GoodsCategory record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int insertSelective(GoodsCategory record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    List<GoodsCategory> selectByExample(GoodsCategoryExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    GoodsCategory selectByPrimaryKey(Integer id);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExampleSelective(@Param("record") GoodsCategory record, @Param("example") GoodsCategoryExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByExample(@Param("record") GoodsCategory record, @Param("example") GoodsCategoryExample example);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKeySelective(GoodsCategory record);

    /**
     *
     * @mbg.generated 2019-03-20
     */
    int updateByPrimaryKey(GoodsCategory record);
}