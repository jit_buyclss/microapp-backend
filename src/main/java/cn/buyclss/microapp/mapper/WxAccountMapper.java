package cn.buyclss.microapp.mapper;

import cn.buyclss.microapp.entity.domain.WxAccount;
import cn.buyclss.microapp.entity.domain.WxAccountExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;


public interface WxAccountMapper {
    WxAccount selectWxAccountByOpenId(String openid);

    long countByExample(WxAccountExample example);

    int deleteByExample(WxAccountExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(WxAccount record);

    int insertSelective(WxAccount record);

    List<WxAccount> selectByExample(WxAccountExample example);

    WxAccount selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") WxAccount record, @Param("example") WxAccountExample example);

    int updateByExample(@Param("record") WxAccount record, @Param("example") WxAccountExample example);

    int updateByPrimaryKeySelective(WxAccount record);

    int updateByPrimaryKey(WxAccount record);
}