package cn.buyclss.microapp.controller;

import cn.buyclss.microapp.entity.dto.Token;
import cn.buyclss.microapp.entity.support.RespResult;
import cn.buyclss.microapp.entity.vo.WechatLoginVO;
import cn.buyclss.microapp.service.WxAppletLoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.AuthenticationException;

/**
 * @author: Cecurio
 * @create: 2019-02-25 16:13
 **/
@RestController
@Api(value = "小程序登录", description = "小程序登录")
public class LoginController {

    private Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private WxAppletLoginService wxAppletLoginService;

    @PostMapping("/login")
    @ApiOperation(value = "小程序登录",notes = "小程序登录，使用code登录")
    public RespResult login(@RequestBody WechatLoginVO loginVO) throws AuthenticationException {
        log.info("登录入参: " + loginVO.toString());
        RespResult res = wxAppletLoginService.wxUserLogin(loginVO.getCode());
        return res;
    }



    // TODO 下单
    // TODO 商家端查看
    // TODO 发货
    // TODO 小程序查看订单状态
    // TODO 确认收货
    // TODO 评价


}
