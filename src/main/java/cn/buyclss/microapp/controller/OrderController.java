package cn.buyclss.microapp.controller;

import cn.buyclss.microapp.entity.support.RespCode;
import cn.buyclss.microapp.entity.support.RespResult;
import cn.buyclss.microapp.entity.vo.OrderVO;
import cn.buyclss.microapp.service.OrderService;
import cn.buyclss.microapp.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: Cecurio
 * @create: 2019-02-25 16:49
 **/
@RestController
@RequestMapping("/order")
@Api(value = "订单模块", description = "订单模块")
public class OrderController {


    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private OrderService orderService;

    @PostMapping("/addOne")
    @RequiresAuthentication
    @ApiOperation(value = "下单", notes = "下单")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "header", dataType = "string",
            name = "Authorization", value = "访问凭证", required = true),
    })
    public RespResult addOne(@RequestBody OrderVO orderVO) {
        // 获取用户名 map.put("user", userInfoService.getUserInfo());
        return orderService.addOneOrder(orderVO);
    }

    @GetMapping("/my")
    @RequiresAuthentication
    @ApiOperation(value = "我的订单", notes = "我的订单")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "header", name = "Authorization", value = "访问凭证", required = true),
        @ApiImplicitParam(paramType = "query", name = "pageIndex", value = "分页：第几页，默认为1", required = true),
        @ApiImplicitParam(paramType = "query", name = "pageSize", value = "分页：每页多少条，默认为10", required = true)
    })
    public RespResult getMyOrders(Integer pageIndex, Integer pageSize) {
        return orderService.getMyOrders(pageIndex, pageSize);
    }


    @PostMapping("/confirmPay")
    @RequiresAuthentication
    @ApiOperation(value = "确认支付", notes = "确认支付")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "header", dataType = "string",
            name = "Authorization", value = "访问凭证", required = true),
        @ApiImplicitParam(paramType = "query", dataType = "Integer", name = "orderId", value = "订单ID", required = true)
    })
    public RespResult confirmPay(Integer orderId) {
        return orderService.confirmPay(orderId);
    }

    @PostMapping("/confirmGoodsAccepted")
    @RequiresAuthentication
    @ApiOperation(value = "确认收货", notes = "确认收货")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "header", dataType = "string",
            name = "Authorization", value = "访问凭证", required = true),
        @ApiImplicitParam(paramType = "query", dataType = "Integer", name = "orderId", value = "订单ID", required = true)
    })
    public RespResult confirmGoodsAccepted(Integer orderId) {
        return orderService.confirmGoodsAccepted(orderId);
    }


}
