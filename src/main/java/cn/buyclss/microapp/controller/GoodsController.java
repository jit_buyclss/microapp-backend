package cn.buyclss.microapp.controller;

import cn.buyclss.microapp.entity.support.RespResult;
import cn.buyclss.microapp.entity.vo.PageVO;
import cn.buyclss.microapp.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: Cecurio
 * @create: 2019-03-20 22:04
 **/
@RestController
@RequestMapping("/goods")
@Api(value = "商品模块", description = "商品模块")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    @GetMapping("/forMe")
    @ApiOperation(value = "查看商品", notes = "查看商品，首页放置，也从推荐算法模块取得，如果刚刚注册，则显示系统目前热销的商品")
    @RequiresAuthentication
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "header", name = "Authorization", required = true),
        @ApiImplicitParam(paramType = "query", name = "pageIndex", value = "分页：第几页，默认为1", required = true),
        @ApiImplicitParam(paramType = "query", name = "pageSize", value = "分页：每页多少条，默认为10", required = true)
    })
    public RespResult getMyGoods(@RequestParam(value = "pageIndex", defaultValue = "1") Integer pageIndex,
                                 @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        return goodsService.fetchGoodsAndSpecsForMe(pageIndex, pageSize);
    }


    @GetMapping("/detail")
    @ApiOperation(value = "查看商品", notes = "查看商品，首页放置，也从推荐算法模块取得，如果刚刚注册，则显示系统目前热销的商品")
    @RequiresAuthentication
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "header", name = "Authorization", required = true),
        @ApiImplicitParam(paramType = "query", name = "goodsId", value = "商品详情", required = true)
    })
    public RespResult getGoodsDetail(@RequestParam(value = "goodsId") Integer goodsId) {
        return goodsService.getGoodsDetail(goodsId);
    }

}
