package cn.buyclss.microapp.test;

import xyz.downgoon.snowflake.Snowflake;
import xyz.downgoon.snowflake.util.BinHexUtil;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Cecurio
 * @create: 2019-03-03 13:06
 **/
public class MethodInvokeTest {
    public Object getProperty(Object owner, String fieldName) throws Exception {
        Class ownerClass = owner.getClass();

        Field field = ownerClass.getField(fieldName);


        Object property = field.get(owner);

        return property;
    }

    public static void main(String[] args)  {

//        try {
//            Math math = new Math(3,4);
//            Class mathClass = math.getClass();
//
//            Field[] fields = mathClass.getDeclaredFields();
//
//            Method[] methods = mathClass.getMethods();
//
//            Method sumMethod = mathClass.getMethod("sum");
//            int res = (int) sumMethod.invoke(math);
//
//            Method helloMethod = mathClass.getMethod("sayHello", String.class);
//
//            helloMethod.invoke(math,"shankai");
//
//
//            int a = 0;
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }



        Snowflake snowflake = new Snowflake(2, 5);
        final int idAmout = 1000;
        List<Long> idPool = new ArrayList<Long>(idAmout);
        for (int i = 0; i < idAmout; i++) {
            long id = snowflake.nextId();
            idPool.add(id);
        }

        for (Long id : idPool) {
            System.out.println(String.format("%s => id: %d, hex: %s, bin: %s", snowflake.formatId(id), id,
                BinHexUtil.hex(id), BinHexUtil.bin(id)));
        }


    }
}
