package cn.buyclss.microapp.test;

/**
 * @author: Cecurio
 * @create: 2019-03-03 13:09
 **/
public class Math {
    private int a;
    private int b;

    public Math(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int sum(){
        return a+b;
    }

    public void sayHello(String name){
        System.out.println("hello, " + name);
    }
}
