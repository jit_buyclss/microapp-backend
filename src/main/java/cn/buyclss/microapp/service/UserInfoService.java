package cn.buyclss.microapp.service;

import cn.buyclss.microapp.config.jwt.JwtConfig;
import cn.buyclss.microapp.entity.domain.Customer;
import cn.buyclss.microapp.entity.vo.JwtTokenVO;
import cn.buyclss.microapp.mapper.CustomerMapper;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: Cecurio
 * @create: 2019-03-19 13:39
 **/
@Service
public class UserInfoService {


    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
    private CustomerMapper customerMapper;

    // token -> openId -> Customer
    // 通过token换userInfo
    public Customer getUserInfo() {
        JwtTokenVO token = (JwtTokenVO) SecurityUtils.getSubject().getPrincipal();
        String openid = jwtConfig.getWxOpenIdByToken(token.getToken());
        Customer customer = customerMapper.selectCustomerByOpenId(openid);
        return customer;
    }


}
