package cn.buyclss.microapp.service;

import cn.buyclss.microapp.entity.support.RespResult;

/**
 * @author: Cecurio
 * @create: 2019-03-20 22:02
 **/
public interface GoodsService {
    RespResult fetchGoodsAndSpecsForMe(Integer pageIndex, Integer pageSize);

    RespResult getGoodsDetail(Integer goodsId);
}
