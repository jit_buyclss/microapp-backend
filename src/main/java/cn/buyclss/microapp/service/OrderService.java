package cn.buyclss.microapp.service;

import cn.buyclss.microapp.entity.support.RespResult;
import cn.buyclss.microapp.entity.vo.OrderVO; /**
 * @author: Cecurio
 * @create: 2019-03-22 12:11
 **/
public interface OrderService {
    RespResult addOneOrder(OrderVO orderVO);

    RespResult confirmPay(Integer orderId);

    RespResult confirmGoodsAccepted(Integer orderId);

    RespResult getMyOrders(Integer pageIndex,Integer pageSize);
}
