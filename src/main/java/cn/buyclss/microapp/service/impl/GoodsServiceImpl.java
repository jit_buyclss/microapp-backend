package cn.buyclss.microapp.service.impl;

import cn.buyclss.microapp.entity.domain.Customer;
import cn.buyclss.microapp.entity.po.GoodsPO;
import cn.buyclss.microapp.entity.po.GoodsWithSpecsDetailPO;
import cn.buyclss.microapp.entity.support.RespResult;
import cn.buyclss.microapp.mapper.GoodsMapper;
import cn.buyclss.microapp.service.GoodsService;
import cn.buyclss.microapp.service.UserInfoService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: Cecurio
 * @create: 2019-03-20 22:03
 **/
@Service
public class GoodsServiceImpl implements GoodsService {
    private Logger logger = LoggerFactory.getLogger(GoodsServiceImpl.class);

    @Autowired
    private UserInfoService userInfoService;

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public RespResult fetchGoodsAndSpecsForMe(Integer pageIndex, Integer pageSize) {
        RespResult res = RespResult.successInstance();
        // 拿到custom_id
        Customer currentCustomer = userInfoService.getUserInfo();
        Integer uid = currentCustomer.getId();
        logger.info("当前用户ID: " + uid);
        //先获取分页次数的商品
        PageHelper.startPage(pageIndex, pageSize);
        List<GoodsPO> goodsPOList = goodsMapper.selectGoodsWithShopAndCategory();
        PageInfo<GoodsPO> pageInfo = new PageInfo<GoodsPO>(goodsPOList);
        res.putValue("goods", pageInfo);
        return res;
    }

    @Override
    public RespResult getGoodsDetail(Integer goodsId) {
        RespResult res= RespResult.successInstance();
        GoodsWithSpecsDetailPO goodsWithSpecsDetailPO = goodsMapper.selectGoodWithSpecs(goodsId);
        res.putValue("goods",goodsWithSpecsDetailPO);
        return res;
    }
}
