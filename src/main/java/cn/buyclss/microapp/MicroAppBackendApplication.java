package cn.buyclss.microapp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * @author: Cecurio
 * @create: 2019-02-25 16:11
 **/
@MapperScan("cn.buyclss.microapp.mapper")
@PropertySource("classpath:documentation.properties")
@SpringBootApplication
public class MicroAppBackendApplication {
    public static void main(String[] args) {
        SpringApplication.run(MicroAppBackendApplication.class, args);
    }
}
