package cn.buyclss.microapp.util;

import com.alibaba.fastjson.JSONObject;

/**
 * @author: Cecurio
 * @create: 2019-02-20 19:19
 **/
public class JSONUtil {
    /**
     * 将JSON字符串转为Java对象
     */
    public static <T> T  jsonString2Object(String result, Class<T> clazz) {
        JSONObject jsonObject = JSONObject.parseObject(result);
        return JSONObject.toJavaObject(jsonObject, clazz);
    }
}
