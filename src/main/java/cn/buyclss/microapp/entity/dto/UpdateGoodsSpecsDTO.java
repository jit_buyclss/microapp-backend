package cn.buyclss.microapp.entity.dto;

/**
 * @author: Cecurio
 * @create: 2019-03-22 13:11
 **/
public class UpdateGoodsSpecsDTO {
    private Integer goodsSpecsId;
    private Integer purchaseCount;

    public Integer getGoodsSpecsId() {
        return goodsSpecsId;
    }

    public void setGoodsSpecsId(Integer goodsSpecsId) {
        this.goodsSpecsId = goodsSpecsId;
    }

    public Integer getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Integer purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    @Override
    public String toString() {
        return "UpdateGoodsSpecsDTO{" +
            "goodsSpecsId=" + goodsSpecsId +
            ", purchaseCount=" + purchaseCount +
            '}';
    }
}
