package cn.buyclss.microapp.entity.dto;

/**
 * @author: Cecurio
 * @create: 2019-03-22 11:25
 **/
public class GoodsSpecsDTO {
    private Integer id;
    private String specsvalue;
    private Long price;
    private  Integer stockNum;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSpecsvalue() {
        return specsvalue;
    }

    public void setSpecsvalue(String specsvalue) {
        this.specsvalue = specsvalue;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }
}
