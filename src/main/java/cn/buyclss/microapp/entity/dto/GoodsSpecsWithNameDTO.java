package cn.buyclss.microapp.entity.dto;

/**
 * @author: Cecurio
 * @create: 2019-03-22 18:05
 **/
public class GoodsSpecsWithNameDTO {
    private String goodsName;
    private String goodsDescription;
    private String goodsDetail;
    private String goodsImgList;
    private Integer goodsSpecsId;
    private String goodsSpecsValue;
    private Long goodsSpecsPrice;
    private Integer goodsSpecsCount;

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsDescription() {
        return goodsDescription;
    }

    public void setGoodsDescription(String goodsDescription) {
        this.goodsDescription = goodsDescription;
    }

    public String getGoodsDetail() {
        return goodsDetail;
    }

    public void setGoodsDetail(String goodsDetail) {
        this.goodsDetail = goodsDetail;
    }

    public String getGoodsImgList() {
        return goodsImgList;
    }

    public void setGoodsImgList(String goodsImgList) {
        this.goodsImgList = goodsImgList;
    }

    public Integer getGoodsSpecsId() {
        return goodsSpecsId;
    }

    public void setGoodsSpecsId(Integer goodsSpecsId) {
        this.goodsSpecsId = goodsSpecsId;
    }

    public String getGoodsSpecsValue() {
        return goodsSpecsValue;
    }

    public void setGoodsSpecsValue(String goodsSpecsValue) {
        this.goodsSpecsValue = goodsSpecsValue;
    }

    public Long getGoodsSpecsPrice() {
        return goodsSpecsPrice;
    }

    public void setGoodsSpecsPrice(Long goodsSpecsPrice) {
        this.goodsSpecsPrice = goodsSpecsPrice;
    }

    public Integer getGoodsSpecsCount() {
        return goodsSpecsCount;
    }

    public void setGoodsSpecsCount(Integer goodsSpecsCount) {
        this.goodsSpecsCount = goodsSpecsCount;
    }
}
