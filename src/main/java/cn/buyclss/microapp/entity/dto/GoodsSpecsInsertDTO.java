package cn.buyclss.microapp.entity.dto;

/**
 * @author: Cecurio
 * @create: 2019-03-22 13:27
 **/
public class GoodsSpecsInsertDTO {
    private Integer orderId;
    private Integer specsId;
    private Integer purchaseCount;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getSpecsId() {
        return specsId;
    }

    public void setSpecsId(Integer specsId) {
        this.specsId = specsId;
    }

    public Integer getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Integer purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    @Override
    public String toString() {
        return "GoodsSpecsInsertDTO{" +
            "orderId=" + orderId +
            ", specsId=" + specsId +
            ", purchaseCount='" + purchaseCount + '\'' +
            '}';
    }
}
