package cn.buyclss.microapp.entity.dto;

/**
 * @author: Cecurio
 * @create: 2019-02-25 16:15
 **/
public class Token {
    private String token;

    public Token() {
    }

    public Token(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Token { " +
            "token='" + token + '\'' +
            '}';
    }
}
