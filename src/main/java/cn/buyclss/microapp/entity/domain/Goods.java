package cn.buyclss.microapp.entity.domain;

import java.util.Date;

public class Goods {
    /**
     * 商品编号
     */
    private Integer id;

    /**
     * 商品另起的编号 无规则，随机生成，防止无良爬取
     */
    private String alias;

    /**
     * 商品所属店铺
     */
    private Integer shopId;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品表述
     */
    private String description;

    /**
     * 商品详情
     */
    private String detail;

    /**
     * 图片列表 图片URL列表，以逗号分隔
     */
    private String imgList;

    /**
     * 库存数
     */
    private Integer stockNum;

    /**
     * 商品价格 取该商品所有规格中最便宜的一种
     */
    private Long price;

    /**
     * 促销价格
     */
    private Long promotionPrice;

    /**
     * 商品状态 1:下架2:上架3:违规下架
     */
    private Integer goodsStatus;

    /**
     * 商品种类
     */
    private Integer goodsCategoryId;

    /**
     * 商家给的编号 商家内边编号
     */
    private String code;

    /**
     * 是否被删除
     */
    private Integer deleted;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private Integer updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 商品编号
     * @return id 商品编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 商品编号
     * @param id 商品编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 商品另起的编号 无规则，随机生成，防止无良爬取
     * @return alias 商品另起的编号 无规则，随机生成，防止无良爬取
     */
    public String getAlias() {
        return alias;
    }

    /**
     * 商品另起的编号 无规则，随机生成，防止无良爬取
     * @param alias 商品另起的编号 无规则，随机生成，防止无良爬取
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * 商品所属店铺
     * @return shop_id 商品所属店铺
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 商品所属店铺
     * @param shopId 商品所属店铺
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    /**
     * 商品名称
     * @return name 商品名称
     */
    public String getName() {
        return name;
    }

    /**
     * 商品名称
     * @param name 商品名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 商品表述
     * @return description 商品表述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 商品表述
     * @param description 商品表述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 商品详情
     * @return detail 商品详情
     */
    public String getDetail() {
        return detail;
    }

    /**
     * 商品详情
     * @param detail 商品详情
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * 图片列表 图片URL列表，以逗号分隔
     * @return img_list 图片列表 图片URL列表，以逗号分隔
     */
    public String getImgList() {
        return imgList;
    }

    /**
     * 图片列表 图片URL列表，以逗号分隔
     * @param imgList 图片列表 图片URL列表，以逗号分隔
     */
    public void setImgList(String imgList) {
        this.imgList = imgList;
    }

    /**
     * 库存数
     * @return stock_num 库存数
     */
    public Integer getStockNum() {
        return stockNum;
    }

    /**
     * 库存数
     * @param stockNum 库存数
     */
    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    /**
     * 商品价格 取该商品所有规格中最便宜的一种
     * @return price 商品价格 取该商品所有规格中最便宜的一种
     */
    public Long getPrice() {
        return price;
    }

    /**
     * 商品价格 取该商品所有规格中最便宜的一种
     * @param price 商品价格 取该商品所有规格中最便宜的一种
     */
    public void setPrice(Long price) {
        this.price = price;
    }

    /**
     * 促销价格
     * @return promotion_price 促销价格
     */
    public Long getPromotionPrice() {
        return promotionPrice;
    }

    /**
     * 促销价格
     * @param promotionPrice 促销价格
     */
    public void setPromotionPrice(Long promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    /**
     * 商品状态 1:下架2:上架3:违规下架
     * @return goods_status 商品状态 1:下架2:上架3:违规下架
     */
    public Integer getGoodsStatus() {
        return goodsStatus;
    }

    /**
     * 商品状态 1:下架2:上架3:违规下架
     * @param goodsStatus 商品状态 1:下架2:上架3:违规下架
     */
    public void setGoodsStatus(Integer goodsStatus) {
        this.goodsStatus = goodsStatus;
    }

    /**
     * 商品种类
     * @return goods_category_id 商品种类
     */
    public Integer getGoodsCategoryId() {
        return goodsCategoryId;
    }

    /**
     * 商品种类
     * @param goodsCategoryId 商品种类
     */
    public void setGoodsCategoryId(Integer goodsCategoryId) {
        this.goodsCategoryId = goodsCategoryId;
    }

    /**
     * 商家给的编号 商家内边编号
     * @return code 商家给的编号 商家内边编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 商家给的编号 商家内边编号
     * @param code 商家给的编号 商家内边编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 是否被删除
     * @return deleted 是否被删除
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 是否被删除
     * @param deleted 是否被删除
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * 乐观锁
     * @return revision 乐观锁
     */
    public Integer getRevision() {
        return revision;
    }

    /**
     * 乐观锁
     * @param revision 乐观锁
     */
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    /**
     * 创建人
     * @return created_by 创建人
     */
    public Integer getCreatedBy() {
        return createdBy;
    }

    /**
     * 创建人
     * @param createdBy 创建人
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 更新人
     * @return updated_by 更新人
     */
    public Integer getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 更新人
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * 更新时间
     * @return updated_time 更新时间
     */
    public Date getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 更新时间
     * @param updatedTime 更新时间
     */
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}