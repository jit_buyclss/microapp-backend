package cn.buyclss.microapp.entity.domain;

import java.util.Date;

public class Order {
    /**
     * 自增编号
     */
    private Integer id;

    /**
     * 购买者编号
     */
    private Integer uid;

    /**
     * 商家编号 用户对商家进行评分，订单和商家是1-1的关系
     */
    private Integer shopId;

    /**
     * 买家评分 评分类别为1-5，其中5表示最好。
     */
    private Integer mark;

    /**
     * 是否好评 mark大于等于4为好评，good_mark字段为1；否则为0 .
     */
    private Integer goodMark;

    /**
     * 买家评语 对订单的评语
     */
    private String evaluationWord;

    /**
     * 是否已经评价。1：已经评价，0：未评价
     */
    private Integer marked;

    /**
     * 订单状态 待付款 待发货 待收货 待评价
     */
    private Integer orderStatus;

    /**
     * 是否被删除
     */
    private Integer deleted;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private Integer updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 自增编号
     * @return id 自增编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 自增编号
     * @param id 自增编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 购买者编号
     * @return uid 购买者编号
     */
    public Integer getUid() {
        return uid;
    }

    /**
     * 购买者编号
     * @param uid 购买者编号
     */
    public void setUid(Integer uid) {
        this.uid = uid;
    }

    /**
     * 商家编号 用户对商家进行评分，订单和商家是1-1的关系
     * @return shop_id 商家编号 用户对商家进行评分，订单和商家是1-1的关系
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 商家编号 用户对商家进行评分，订单和商家是1-1的关系
     * @param shopId 商家编号 用户对商家进行评分，订单和商家是1-1的关系
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    /**
     * 买家评分 评分类别为1-5，其中5表示最好。
     * @return mark 买家评分 评分类别为1-5，其中5表示最好。
     */
    public Integer getMark() {
        return mark;
    }

    /**
     * 买家评分 评分类别为1-5，其中5表示最好。
     * @param mark 买家评分 评分类别为1-5，其中5表示最好。
     */
    public void setMark(Integer mark) {
        this.mark = mark;
    }

    /**
     * 是否好评 mark大于等于4为好评，good_mark字段为1；否则为0 .
     * @return good_mark 是否好评 mark大于等于4为好评，good_mark字段为1；否则为0 .
     */
    public Integer getGoodMark() {
        return goodMark;
    }

    /**
     * 是否好评 mark大于等于4为好评，good_mark字段为1；否则为0 .
     * @param goodMark 是否好评 mark大于等于4为好评，good_mark字段为1；否则为0 .
     */
    public void setGoodMark(Integer goodMark) {
        this.goodMark = goodMark;
    }

    /**
     * 买家评语 对订单的评语
     * @return evaluation_word 买家评语 对订单的评语
     */
    public String getEvaluationWord() {
        return evaluationWord;
    }

    /**
     * 买家评语 对订单的评语
     * @param evaluationWord 买家评语 对订单的评语
     */
    public void setEvaluationWord(String evaluationWord) {
        this.evaluationWord = evaluationWord;
    }

    /**
     * 是否已经评价。1：已经评价，0：未评价
     * @return marked 是否已经评价。1：已经评价，0：未评价
     */
    public Integer getMarked() {
        return marked;
    }

    /**
     * 是否已经评价。1：已经评价，0：未评价
     * @param marked 是否已经评价。1：已经评价，0：未评价
     */
    public void setMarked(Integer marked) {
        this.marked = marked;
    }

    /**
     * 订单状态 待付款 待发货 待收货 待评价
     * @return order_status 订单状态 待付款 待发货 待收货 待评价
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }

    /**
     * 订单状态 待付款 待发货 待收货 待评价
     * @param orderStatus 订单状态 待付款 待发货 待收货 待评价
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 是否被删除
     * @return deleted 是否被删除
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 是否被删除
     * @param deleted 是否被删除
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * 乐观锁
     * @return revision 乐观锁
     */
    public Integer getRevision() {
        return revision;
    }

    /**
     * 乐观锁
     * @param revision 乐观锁
     */
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    /**
     * 创建人
     * @return created_by 创建人
     */
    public Integer getCreatedBy() {
        return createdBy;
    }

    /**
     * 创建人
     * @param createdBy 创建人
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 更新人
     * @return updated_by 更新人
     */
    public Integer getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 更新人
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * 更新时间
     * @return updated_time 更新时间
     */
    public Date getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 更新时间
     * @param updatedTime 更新时间
     */
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}