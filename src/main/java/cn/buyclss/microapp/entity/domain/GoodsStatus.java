package cn.buyclss.microapp.entity.domain;

public class GoodsStatus {
    /**
     * 商品状态编号
     */
    private Integer id;

    /**
     * 商品状态名称
     */
    private String name;

    /**
     * 是否被删除
     */
    private Integer deleted;

    /**
     * 商品状态编号
     * @return id 商品状态编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 商品状态编号
     * @param id 商品状态编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 商品状态名称
     * @return name 商品状态名称
     */
    public String getName() {
        return name;
    }

    /**
     * 商品状态名称
     * @param name 商品状态名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 是否被删除
     * @return deleted 是否被删除
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 是否被删除
     * @param deleted 是否被删除
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}