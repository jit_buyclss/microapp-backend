package cn.buyclss.microapp.entity.domain;

import java.util.Date;

public class Shop {
    /**
     * 商铺编号
     */
    private Integer id;

    /**
     * 商家编号 对应p_user表的用户
     */
    private Integer ownId;

    /**
     * 商铺名称
     */
    private String name;

    /**
     * 商铺简介
     */
    private String introduction;

    /**
     * 商家logo
     */
    private String logo;

    /**
     * 商家地址
     */
    private String address;

    /**
     * 商品总数
     */
    private Integer goodsCount;

    /**
     * 订单总数
     */
    private Integer orderCount;

    /**
     * 好评次数 好评次数
     */
    private Integer goodMarkTimes;

    /**
     * 是否被删除
     */
    private Integer deleted;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private Integer updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 商铺编号
     * @return id 商铺编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 商铺编号
     * @param id 商铺编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 商家编号 对应p_user表的用户
     * @return own_id 商家编号 对应p_user表的用户
     */
    public Integer getOwnId() {
        return ownId;
    }

    /**
     * 商家编号 对应p_user表的用户
     * @param ownId 商家编号 对应p_user表的用户
     */
    public void setOwnId(Integer ownId) {
        this.ownId = ownId;
    }

    /**
     * 商铺名称
     * @return name 商铺名称
     */
    public String getName() {
        return name;
    }

    /**
     * 商铺名称
     * @param name 商铺名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 商铺简介
     * @return introduction 商铺简介
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 商铺简介
     * @param introduction 商铺简介
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    /**
     * 商家logo
     * @return logo 商家logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * 商家logo
     * @param logo 商家logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * 商家地址
     * @return address 商家地址
     */
    public String getAddress() {
        return address;
    }

    /**
     * 商家地址
     * @param address 商家地址
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 商品总数
     * @return goods_count 商品总数
     */
    public Integer getGoodsCount() {
        return goodsCount;
    }

    /**
     * 商品总数
     * @param goodsCount 商品总数
     */
    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    /**
     * 订单总数
     * @return order_count 订单总数
     */
    public Integer getOrderCount() {
        return orderCount;
    }

    /**
     * 订单总数
     * @param orderCount 订单总数
     */
    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    /**
     * 好评次数 好评次数
     * @return good_mark_times 好评次数 好评次数
     */
    public Integer getGoodMarkTimes() {
        return goodMarkTimes;
    }

    /**
     * 好评次数 好评次数
     * @param goodMarkTimes 好评次数 好评次数
     */
    public void setGoodMarkTimes(Integer goodMarkTimes) {
        this.goodMarkTimes = goodMarkTimes;
    }

    /**
     * 是否被删除
     * @return deleted 是否被删除
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 是否被删除
     * @param deleted 是否被删除
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * 乐观锁
     * @return revision 乐观锁
     */
    public Integer getRevision() {
        return revision;
    }

    /**
     * 乐观锁
     * @param revision 乐观锁
     */
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    /**
     * 创建人
     * @return created_by 创建人
     */
    public Integer getCreatedBy() {
        return createdBy;
    }

    /**
     * 创建人
     * @param createdBy 创建人
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 更新人
     * @return updated_by 更新人
     */
    public Integer getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 更新人
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * 更新时间
     * @return updated_time 更新时间
     */
    public Date getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 更新时间
     * @param updatedTime 更新时间
     */
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}