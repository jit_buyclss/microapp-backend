package cn.buyclss.microapp.entity.domain;

import java.util.Date;

public class GoodsCategory {
    /**
     * 种类编号
     */
    private Integer id;

    /**
     * 种类名字
     */
    private String name;

    /**
     * 父级种类编号
     */
    private Integer parentId;

    /**
     * 排序索引 0-10，其值越小展示时越往前
     */
    private Integer sortIndex;

    /**
     * 是否被删除 1:被删除,0:未被删除
     */
    private Integer deleted;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private Integer updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 种类编号
     * @return id 种类编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 种类编号
     * @param id 种类编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 种类名字
     * @return name 种类名字
     */
    public String getName() {
        return name;
    }

    /**
     * 种类名字
     * @param name 种类名字
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 父级种类编号
     * @return parent_id 父级种类编号
     */
    public Integer getParentId() {
        return parentId;
    }

    /**
     * 父级种类编号
     * @param parentId 父级种类编号
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 排序索引 0-10，其值越小展示时越往前
     * @return sort_index 排序索引 0-10，其值越小展示时越往前
     */
    public Integer getSortIndex() {
        return sortIndex;
    }

    /**
     * 排序索引 0-10，其值越小展示时越往前
     * @param sortIndex 排序索引 0-10，其值越小展示时越往前
     */
    public void setSortIndex(Integer sortIndex) {
        this.sortIndex = sortIndex;
    }

    /**
     * 是否被删除 1:被删除,0:未被删除
     * @return deleted 是否被删除 1:被删除,0:未被删除
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 是否被删除 1:被删除,0:未被删除
     * @param deleted 是否被删除 1:被删除,0:未被删除
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * 乐观锁
     * @return revision 乐观锁
     */
    public Integer getRevision() {
        return revision;
    }

    /**
     * 乐观锁
     * @param revision 乐观锁
     */
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    /**
     * 创建人
     * @return created_by 创建人
     */
    public Integer getCreatedBy() {
        return createdBy;
    }

    /**
     * 创建人
     * @param createdBy 创建人
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 更新人
     * @return updated_by 更新人
     */
    public Integer getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 更新人
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * 更新时间
     * @return updated_time 更新时间
     */
    public Date getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 更新时间
     * @param updatedTime 更新时间
     */
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}