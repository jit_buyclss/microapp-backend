package cn.buyclss.microapp.entity.domain;

import java.util.Date;

public class GoodsSpecs {
    /**
     * 自增编号
     */
    private Integer id;

    /**
     * 店铺的编号 冗余goods表的shop_id
     */
    private Integer shopId;

    /**
     * 商品的编号
     */
    private Integer goodsId;

    /**
     * 规格值
     */
    private String specValue;

    /**
     * 价格/分 价格以分为单位，1元=100分，1角=10分
     */
    private Long price;

    /**
     * 库存数
     */
    private Integer stockNum;

    /**
     * 是否被删除
     */
    private Integer deleted;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private Integer updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 自增编号
     * @return id 自增编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 自增编号
     * @param id 自增编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 店铺的编号 冗余goods表的shop_id
     * @return shop_id 店铺的编号 冗余goods表的shop_id
     */
    public Integer getShopId() {
        return shopId;
    }

    /**
     * 店铺的编号 冗余goods表的shop_id
     * @param shopId 店铺的编号 冗余goods表的shop_id
     */
    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    /**
     * 商品的编号
     * @return goods_id 商品的编号
     */
    public Integer getGoodsId() {
        return goodsId;
    }

    /**
     * 商品的编号
     * @param goodsId 商品的编号
     */
    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    /**
     * 规格值
     * @return spec_value 规格值
     */
    public String getSpecValue() {
        return specValue;
    }

    /**
     * 规格值
     * @param specValue 规格值
     */
    public void setSpecValue(String specValue) {
        this.specValue = specValue;
    }

    /**
     * 价格/分 价格以分为单位，1元=100分，1角=10分
     * @return price 价格/分 价格以分为单位，1元=100分，1角=10分
     */
    public Long getPrice() {
        return price;
    }

    /**
     * 价格/分 价格以分为单位，1元=100分，1角=10分
     * @param price 价格/分 价格以分为单位，1元=100分，1角=10分
     */
    public void setPrice(Long price) {
        this.price = price;
    }

    /**
     * 库存数
     * @return stock_num 库存数
     */
    public Integer getStockNum() {
        return stockNum;
    }

    /**
     * 库存数
     * @param stockNum 库存数
     */
    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    /**
     * 是否被删除
     * @return deleted 是否被删除
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 是否被删除
     * @param deleted 是否被删除
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * 乐观锁
     * @return revision 乐观锁
     */
    public Integer getRevision() {
        return revision;
    }

    /**
     * 乐观锁
     * @param revision 乐观锁
     */
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    /**
     * 创建人
     * @return created_by 创建人
     */
    public Integer getCreatedBy() {
        return createdBy;
    }

    /**
     * 创建人
     * @param createdBy 创建人
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 更新人
     * @return updated_by 更新人
     */
    public Integer getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 更新人
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * 更新时间
     * @return updated_time 更新时间
     */
    public Date getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 更新时间
     * @param updatedTime 更新时间
     */
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}