package cn.buyclss.microapp.entity.domain;

import java.util.Date;

public class OrderGoodsSpecs {
    /**
     * 自增编号
     */
    private Integer id;

    /**
     * 订单编号
     */
    private Integer orderId;

    /**
     * 物品规格编码
     */
    private Integer goodsSpecsId;

    /**
     * 物品的数量
     */
    private Integer goodsCount;

    /**
     * 商品评分
     */
    private Integer mark;

    /**
     * 评语 商品评语
     */
    private String evaluationWord;

    /**
     * 是否被删除
     */
    private Integer deleted;

    /**
     * 乐观锁
     */
    private Integer revision;

    /**
     * 创建人
     */
    private Integer createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private Integer updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 自增编号
     * @return id 自增编号
     */
    public Integer getId() {
        return id;
    }

    /**
     * 自增编号
     * @param id 自增编号
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 订单编号
     * @return order_id 订单编号
     */
    public Integer getOrderId() {
        return orderId;
    }

    /**
     * 订单编号
     * @param orderId 订单编号
     */
    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    /**
     * 物品规格编码
     * @return goods_specs_id 物品规格编码
     */
    public Integer getGoodsSpecsId() {
        return goodsSpecsId;
    }

    /**
     * 物品规格编码
     * @param goodsSpecsId 物品规格编码
     */
    public void setGoodsSpecsId(Integer goodsSpecsId) {
        this.goodsSpecsId = goodsSpecsId;
    }

    /**
     * 物品的数量
     * @return goods_count 物品的数量
     */
    public Integer getGoodsCount() {
        return goodsCount;
    }

    /**
     * 物品的数量
     * @param goodsCount 物品的数量
     */
    public void setGoodsCount(Integer goodsCount) {
        this.goodsCount = goodsCount;
    }

    /**
     * 商品评分
     * @return mark 商品评分
     */
    public Integer getMark() {
        return mark;
    }

    /**
     * 商品评分
     * @param mark 商品评分
     */
    public void setMark(Integer mark) {
        this.mark = mark;
    }

    /**
     * 评语 商品评语
     * @return evaluation_word 评语 商品评语
     */
    public String getEvaluationWord() {
        return evaluationWord;
    }

    /**
     * 评语 商品评语
     * @param evaluationWord 评语 商品评语
     */
    public void setEvaluationWord(String evaluationWord) {
        this.evaluationWord = evaluationWord;
    }

    /**
     * 是否被删除
     * @return deleted 是否被删除
     */
    public Integer getDeleted() {
        return deleted;
    }

    /**
     * 是否被删除
     * @param deleted 是否被删除
     */
    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    /**
     * 乐观锁
     * @return revision 乐观锁
     */
    public Integer getRevision() {
        return revision;
    }

    /**
     * 乐观锁
     * @param revision 乐观锁
     */
    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    /**
     * 创建人
     * @return created_by 创建人
     */
    public Integer getCreatedBy() {
        return createdBy;
    }

    /**
     * 创建人
     * @param createdBy 创建人
     */
    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * 创建时间
     * @return created_time 创建时间
     */
    public Date getCreatedTime() {
        return createdTime;
    }

    /**
     * 创建时间
     * @param createdTime 创建时间
     */
    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    /**
     * 更新人
     * @return updated_by 更新人
     */
    public Integer getUpdatedBy() {
        return updatedBy;
    }

    /**
     * 更新人
     * @param updatedBy 更新人
     */
    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * 更新时间
     * @return updated_time 更新时间
     */
    public Date getUpdatedTime() {
        return updatedTime;
    }

    /**
     * 更新时间
     * @param updatedTime 更新时间
     */
    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}