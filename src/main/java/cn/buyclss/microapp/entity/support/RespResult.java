package cn.buyclss.microapp.entity.support;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回客户端的JSON对应的类
 *
 * @author: Cecurio
 * @create: 2019-02-19 9:29
 **/
public class RespResult {
    /**
     * 1000 成功 客户端渲染页面
     * 1001 未登录跳转到登录页
     * 1002 失败, 提示错误信息
     */
    private int code;
    private String errMsg;
    private Map<String, Object> data;


    public RespResult() {
    }

    public RespResult(int code, String errMsg, Map<String, Object> data) {
        this.code = code;
        this.errMsg = errMsg;
        this.data = data;
    }

    public static RespResult successInstance() {
        Map<String, Object> data = new HashMap<>();
        return new RespResult(RespCode.SUCCESS, "", data);
    }

    public static RespResult failureInstance(String errMsg) {
        return new RespResult(RespCode.FAILURE, errMsg, null);
    }

    public static RespResult failureInstance() {
        return new RespResult(RespCode.FAILURE, null, null);
    }

    public void putValue(String k, Object v) {
        if (this.data != null) {
            this.data.put(k, v);
        }
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }
}
