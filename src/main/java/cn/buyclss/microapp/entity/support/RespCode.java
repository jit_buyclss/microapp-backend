package cn.buyclss.microapp.entity.support;

/**
 * @author: Cecurio
 * @create: 2019-02-20 13:45
 **/
public class RespCode {

    /**
     * 成功 客户端渲染页面
     */
    public static final Integer SUCCESS = 1000;
    /**
     * 未登录跳转到登录页
     */
    public static final Integer UN_LOGIN = 1001;
    /**
     * 失败, 提示错误信息
     */
    public static final Integer FAILURE = 1002;

}
