package cn.buyclss.microapp.entity.vo;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author: Cecurio
 * @create: 2019-02-21 0:14
 **/
public class JwtTokenVO implements AuthenticationToken {

    private String token;

    public JwtTokenVO(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
