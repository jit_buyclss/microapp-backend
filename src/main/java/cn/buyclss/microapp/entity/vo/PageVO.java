package cn.buyclss.microapp.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: Cecurio
 * @create: 2019-03-20 21:21
 **/
@ApiModel
public class PageVO {
    @ApiModelProperty(value = "第几页")
    private Integer pageIndex;

    @ApiModelProperty(value = "每页的数量")
    private Integer pageSize;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
