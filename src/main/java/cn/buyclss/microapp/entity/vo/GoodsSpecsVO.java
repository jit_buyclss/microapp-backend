package cn.buyclss.microapp.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author: Cecurio
 * @create: 2019-03-22 11:12
 **/
@ApiModel
public class GoodsSpecsVO {
    @ApiModelProperty("规格编号")
    private Integer goodsSpecsId;

    @ApiModelProperty("购买数量")
    private Integer purchaseCount;

    public Integer getGoodsSpecsId() {
        return goodsSpecsId;
    }

    public void setGoodsSpecsId(Integer goodsSpecsId) {
        this.goodsSpecsId = goodsSpecsId;
    }

    public Integer getPurchaseCount() {
        return purchaseCount;
    }

    public void setPurchaseCount(Integer purchaseCount) {
        this.purchaseCount = purchaseCount;
    }

    @Override
    public String toString() {
        return "GoodsSpecsVO{" +
            "goodsSpecsId=" + goodsSpecsId +
            ", purchaseCount=" + purchaseCount +
            '}';
    }
}
