package cn.buyclss.microapp.entity.vo;

/**
 * @author: Cecurio
 * @create: 2019-03-08 19:43
 **/
public class WechatLoginVO {
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "WechatLoginVO{" +
            "code='" + code + '\'' +
            '}';
    }
}
