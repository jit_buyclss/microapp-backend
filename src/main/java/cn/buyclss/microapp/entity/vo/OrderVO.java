package cn.buyclss.microapp.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author: Cecurio
 * @create: 2019-03-22 11:10
 **/
@ApiModel
public class OrderVO {

    @ApiModelProperty("商铺ID")
    private Integer shopId;

    List<GoodsSpecsVO> specsList;

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public List<GoodsSpecsVO> getSpecsList() {
        return specsList;
    }

    public void setSpecsList(List<GoodsSpecsVO> specsList) {
        this.specsList = specsList;
    }

    @Override
    public String toString() {
        return "OrderVO{" +
            "shopId=" + shopId +
            ", specsList=" + specsList +
            '}';
    }
}
