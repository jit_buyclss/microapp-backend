package cn.buyclss.microapp.entity.po;

import cn.buyclss.microapp.entity.dto.GoodsSpecsDTO;
import cn.buyclss.microapp.entity.dto.ShopDTO;

import java.util.List;

/**
 * @author: Cecurio
 * @create: 2019-03-22 11:05
 **/
public class GoodsWithSpecsDetailPO {

    private ShopDTO shop;
    private String goodsName;
    private String imgList;
    private String description;
    private String detail;
    private String goodsCategory;
    private List<GoodsSpecsDTO> goodsSpecsList;

    public ShopDTO getShop() {
        return shop;
    }

    public void setShop(ShopDTO shop) {
        this.shop = shop;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public List<GoodsSpecsDTO> getGoodsSpecsList() {
        return goodsSpecsList;
    }

    public void setGoodsSpecsList(List<GoodsSpecsDTO> goodsSpecsList) {
        this.goodsSpecsList = goodsSpecsList;
    }

    public String getImgList() {
        return imgList;
    }

    public void setImgList(String imgList) {
        this.imgList = imgList;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getGoodsCategory() {
        return goodsCategory;
    }

    public void setGoodsCategory(String goodsCategory) {
        this.goodsCategory = goodsCategory;
    }
}
