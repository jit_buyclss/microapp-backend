package cn.buyclss.microapp.entity.po;

import cn.buyclss.microapp.entity.dto.GoodsSpecsWithNameDTO;
import cn.buyclss.microapp.entity.dto.ShopDTO;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.util.List;

/**
 * @author: Cecurio
 * @create: 2019-03-22 17:47
 **/
public class OrdersWithShopAndSpecsAndStatusPO {


    private Integer orderId;
    private Integer marked;
    private Integer mark;
    private String evaluationWord;
    private String orderStatusValue;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdTime;
    private ShopDTO shop;
    private List<GoodsSpecsWithNameDTO> goodsSpecsList;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getMarked() {
        return marked;
    }

    public void setMarked(Integer marked) {
        this.marked = marked;
    }

    public Integer getMark() {
        return mark;
    }

    public void setMark(Integer mark) {
        this.mark = mark;
    }

    public String getEvaluationWord() {
        return evaluationWord;
    }

    public void setEvaluationWord(String evaluationWord) {
        this.evaluationWord = evaluationWord;
    }

    public String getOrderStatusValue() {
        return orderStatusValue;
    }

    public void setOrderStatusValue(String orderStatusValue) {
        this.orderStatusValue = orderStatusValue;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public ShopDTO getShop() {
        return shop;
    }

    public void setShop(ShopDTO shop) {
        this.shop = shop;
    }

    public List<GoodsSpecsWithNameDTO> getGoodsSpecsList() {
        return goodsSpecsList;
    }

    public void setGoodsSpecsList(List<GoodsSpecsWithNameDTO> goodsSpecsList) {
        this.goodsSpecsList = goodsSpecsList;
    }
}
