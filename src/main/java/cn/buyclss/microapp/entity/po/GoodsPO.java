package cn.buyclss.microapp.entity.po;

import cn.buyclss.microapp.entity.dto.GoodsCategoryDTO;
import cn.buyclss.microapp.entity.dto.GoodsStatusDTO;
import cn.buyclss.microapp.entity.dto.ShopDTO;

import java.util.Date;

/**
 * @author: Cecurio
 * @create: 2019-03-20 22:20
 **/
public class GoodsPO {
    /**
     * 商品编号
     */
    private Integer id;

    /**
     * 商品另起的编号 无规则，随机生成，防止无良爬取
     */
    private String alias;

    /**
     * 商品所属店铺
     */
    private ShopDTO shop;

    /**
     * 商品名称
     */
    private String name;

    /**
     * 商品表述
     */
    private String description;

    /**
     * 商品详情
     */
    private String detail;

    /**
     * 图片列表 图片URL列表，以逗号分隔
     */
    private String imgList;

    /**
     * 库存数
     */
    private Integer stockNum;

    /**
     * 商品价格 取该商品所有规格中最便宜的一种
     */
    private Long price;

    /**
     * 促销价格
     */
    private Long promotionPrice;


    /**
     * 商品种类
     */
    private GoodsCategoryDTO goodsCategory;

    /**
     * 商家给的编号 商家内边编号
     */
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public ShopDTO getShop() {
        return shop;
    }

    public void setShop(ShopDTO shop) {
        this.shop = shop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getImgList() {
        return imgList;
    }

    public void setImgList(String imgList) {
        this.imgList = imgList;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getPromotionPrice() {
        return promotionPrice;
    }

    public void setPromotionPrice(Long promotionPrice) {
        this.promotionPrice = promotionPrice;
    }

    public GoodsCategoryDTO getGoodsCategory() {
        return goodsCategory;
    }

    public void setGoodsCategory(GoodsCategoryDTO goodsCategory) {
        this.goodsCategory = goodsCategory;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
